﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using XFactor.DAL;

namespace XFactor.Tools
{
    public static class Tools
    {
        public static string ToEUDateFormat(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy").Replace('-', '/');
        }

        public static string ToEUDateTimeFormat(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy hh:mm:ss").Replace('-', '/');
        }

        public static string ToEUDateFormat(this DateTime? date)
        {
            if (date == null)
                return null;

            DateTime realDate = (DateTime)date;
            return realDate.ToString("dd/MM/yyyy").Replace('-', '/');
        }

        public static string ToEUDateTimeFormat(this DateTime? date)
        {
            if (date == null)
                return null;

            DateTime realDate = (DateTime)date;
            return realDate.ToString("dd/MM/yyyy hh:mm:ss").Replace('-', '/');
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source, Random rng)
        {
            T[] elements = source.ToArray();
            for (int i = elements.Length - 1; i >= 0; i--)
            {
                int swapIndex = rng.Next(i + 1);
                yield return elements[swapIndex];
                elements[swapIndex] = elements[i];
            }
        }

        public static Dictionary<string, string> DecodePayload(string payload)
        {
            //Remove the bad part of signed_request
            string[] sB64String = payload.Split('.');
            payload = payload.Replace((sB64String[0] + "."), string.Empty);

            var encoding = new UTF8Encoding();
            var decodedJson = payload.Replace("=", string.Empty).Replace('-', '+').Replace('_', '/');
            var base64JsonArray = Convert.FromBase64String(decodedJson.PadRight(decodedJson.Length + (4 - decodedJson.Length % 4) % 4, '='));
            var json = encoding.GetString(base64JsonArray);
            var jObject = JObject.Parse(json);
            var parameters = new Dictionary<string, string>();
            parameters.Add("like", ((bool)jObject["page"]["liked"]).ToString());
            parameters.Add("admin", ((bool)jObject["page"]["admin"]).ToString());
            return parameters;
        }

        public static bool IsValidGuidFormat(this string value)
        {
            Guid guid = Guid.Empty;
            return Guid.TryParse(value, out guid);
        }

        public static string DownloadFile(string src, string singerID, string fbID) {
            Uri remoteImgPathUri = new Uri(src);
            string remoteImgPathWithoutQuery = remoteImgPathUri.GetLeftPart(UriPartial.Path);
            string fileName = Path.GetFileName(remoteImgPathWithoutQuery);
            string localPath = ConfigurationManager.AppSettings["Profiles"] + singerID + "\\" + fbID + ".jpg";
            WebClient webClient = new WebClient();
            webClient.DownloadFile(src, localPath);
            return localPath;
        }

        internal static void CreateFinal(string singerName, string singerID, string fbID)
        {
            string mosaicSource = ConfigurationManager.AppSettings["Temp"] + singerID + "_" + fbID + ".jpg";
            string alphaSource = ConfigurationManager.AppSettings["Source"] + singerID + "x.jpg";
            string destination = ConfigurationManager.AppSettings["Destination"] + singerID + ".jpg";
            
            int total = 0;
            List<XFactor_Users> users = UsersDAL.All(out total, null, null, null, null, null, "0", "ID", DAL.Helpers.DALHelpers.SortDirection.Asc);
            total = users.Where(item => item.Singer == singerName).Count();
            int rows = (int)Math.Ceiling((double)total / 5);
            using (System.Drawing.Image g = System.Drawing.Bitmap.FromFile(mosaicSource))
            {
                mosaicSource = mosaicSource.Replace(".jpg", "_.jpg");
                g.CropY(960, rows*30).TypeAwareSave(mosaicSource);
            }
            Combine(mosaicSource, alphaSource, destination, rows*30);
        }
        public static System.Drawing.Bitmap Combine(string mosaic, string alpha, string destination, int pixels)
        {
            //read all images into memory
            List<System.Drawing.Bitmap> images = new List<System.Drawing.Bitmap>();
            System.Drawing.Bitmap finalImage = null;
            List<string> files = new List<string>();
            files.Add(alpha);
            files.Add(mosaic);

            try
            {
                int width = 0;
                int height = 0;

                foreach (string image in files)
                {
                    //create a Bitmap from the file and add it to the list
                    System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(image);

                    //update the size of the final bitmap
                    width = bitmap.Width > width ? bitmap.Width : width;
                    height = bitmap.Height > height ? bitmap.Height : height;

                    images.Add(bitmap);
                }

                //create a bitmap to hold the combined image
                finalImage = new System.Drawing.Bitmap(width, height);

                //get a graphics object from the image so we can draw on it
                using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(finalImage))
                {
                    //set background color
                    g.Clear(System.Drawing.Color.Black);

                    //go through each image and draw it on the final image
                    int offset = 0;
                    foreach (System.Drawing.Bitmap image in images)
                    {
                        g.DrawImage(image,
                          new System.Drawing.Rectangle(0, 0, image.Width, image.Height));
                    }
                }

                finalImage.TypeAwareSave(destination);

                return finalImage;
            }
            catch (Exception ex)
            {
                if (finalImage != null)
                    finalImage.Dispose();

                throw ex;
            }
            finally
            {
                //clean up memory
                foreach (System.Drawing.Bitmap image in images)
                {
                    image.Dispose();
                }
            }
        }

        private static Image CropImage(Image img, int width, int height)
        {

            Bitmap bmpImage = new Bitmap(img);

            Bitmap target = new Bitmap(width, height);

            //int x = (size.Width - img.Width) / 2;

            //int y = (size.Height - img.Height) / 2;

            using (Graphics g = Graphics.FromImage(target))
            {

                g.Clear(Color.White);

                g.DrawImage(img, new Rectangle(0, height, img.Width, img.Height), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);

            }

            return (Image)(target);

        }
    }
}