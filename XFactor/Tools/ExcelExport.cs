﻿using XFactor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XFactor.Models;

namespace XFactor.Tools
{
    public class ExcelExport
    {
        protected static string TableWrapper
        {
            get
            {
                string wrapper = "";
                wrapper +=
                    "<html xmlns:o='urn:schemas-microsoft-com:office:office'" +
                    "xmlns:x='urn:schemas-microsoft-com:office:excel'" +
                    "xmlns='http://www.w3.org/TR/REC-html40'>" +
                    "<head>" +
                    "<meta http-equiv=Content-Type content='text/html; charset=UTF-8'/>" +
                    "<meta name=ProgId content=Excel.Sheet/>" +
                    "<meta name=Generator content='Microsoft Excel 11'/>" +
                    "<!--[if gte mso 9]><xml>" +
                     "<x:excelworkbook>" +
                    "<x:excelworksheets>" +
                       "<x:excelworksheet>" +
                    "<x:name>** איסתא **</x:name>" +
                    "<x:worksheetoptions>" +
                        "<x:selected></x:selected>" +
                        "<x:freezepanes></x:freezepanes>" +
                        "<x:frozennosplit></x:frozennosplit>" +
                        "<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>" +
                        "<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>" +
                        "<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>" +
                        "<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>" +
                        "<x:activepane>0</x:activepane>" +
                        "<x:panes>" +
                        "<x:pane>" +
                        "<x:number>3</x:number>" +
                        "</x:pane>" +
                        "<x:pane>" +
                        "<x:number>1</x:number>" +
                        "</x:pane>" +
                        "<x:pane>" +
                        "<x:number>2</x:number>" +
                        "</x:pane>" +
                        "<x:pane>" +
                           "<x:number>0</x:number>" +
                        "</x:pane>" +
                        "</x:panes>" +
                        "<x:protectcontents>False</x:protectcontents>" +
                        "<x:protectobjects>False</x:protectobjects>" +
                        "<x:protectscenarios>False</x:protectscenarios>" +
                    "</x:worksheetoptions>" +
                    "</x:excelworksheet>" +
                    "</x:excelworksheets>" +
                    "<x:protectstructure>False</x:protectstructure>" +
                    "<x:protectwindows>False</x:protectwindows>" +
                    "</x:excelworkbook>" +
                    "</xml>" +
                    "<![endif]-->" +
                    "</head>" +
                    "<body>";




                wrapper +=
                "<table style='font-family: Arial'>" +
                    "<thead>" +
                        "<tr style='background: gray;'>" +
                            "<DATA-HEADERS>" +
                        "</tr>" +
                    "</thead>" +
                    "<tbody>" +
                        "<DATA-ROWS>" +
                    "</tbody>" +
                "</table>";



                wrapper +=
                    "</body></html>";

                return wrapper;
            }
        }

        protected static string TableHeaders
        {
            get
            {
                return
                    "<th><b>מזהה</b></th>" +
                    "<th><b>שם מלא</b></th>" +
                    "<th><b>אי מייל</b></th>" +
                    "<th><b>טלפון</b></th>" +
                    "<th><b>תאריך הרשמה</b></th>" +
                    "<th><b>פייסבוק</b></th>";
            }
        }

        public static string FormatData(IEnumerable<UserView> Users)
        {
            string style = null;
            int i = 0;
            string dataRows = "";
            foreach (var user in Users)
            {
                style = string.Empty;

                if (i % 2 == 1)
                {
                    style = "style='background: #ddd;'";
                }
                i++;
                dataRows +=
                    "<tr " + style + ">" +
                        "<td>" + user.ID + "</td>" +
                        "<td>" + user.CreatedStr + "</td>" +
                        "<td>" + (user.Singer ?? "") + "</td>" +
                        "<td>" + (user.FullName ?? "") + "</td>" +
                        "<td>" + (user.Email ?? "") + "</td>" +
                        "<td>" + (user.Phone ?? "") + "</td>" +
                        "<td>" + user.FacebookID + "</td>";
                dataRows += "</tr>";

            }

            string table = TableWrapper;
            string headers = TableHeaders;
            table = table.Replace("<DATA-ROWS>", dataRows);
            table = table.Replace("<DATA-HEADERS>", headers);
            return table;
        }
    }
}