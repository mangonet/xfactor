﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Data.Entity;

namespace XFactor.DAL.Helpers
{
    public static class DALHelpers
    {
        [Flags]
        public enum SortDirection
        {
            Asc = 1,
            Desc = 2
        }
        public static IQueryable<T> IncludeMulitple<T>(this IQueryable<T> query, IEnumerable<string> includes)
            where T : class
        {
            if (includes != null)
            {
                foreach (string table in includes)
                {
                    query = query.Include(table);
                }
            }
            return query;
        }

        /// <summary>
        /// Include multiple table to object. IF the customIncludes is pass, those will be the included tables, otherwise the default tables will be included.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="defaultIncludes">The default includes</param>
        /// <param name="customIncludes">Custom includes. If no custom includes, should pass null</param>
        /// <returns>query with the included tables</returns>
        public static IQueryable<T> IncludeMulitple<T>(this IQueryable<T> query, IEnumerable<string> defaultIncludes, IEnumerable<string> customIncludes)
            where T : class
        {
            if (customIncludes != null)
            {
                query = query.IncludeMulitple(customIncludes);
            }
            else
            {
                query = query.IncludeMulitple(defaultIncludes);
            }

            return query;
        }

        public static IQueryable<T> SkipTakeClause<T>(this IQueryable<T> query, int skipRows, int takeRows)
               where T : class
        {
            if (skipRows > 0)
            {
                query = query.Skip(skipRows);
            }
            if (takeRows > 0)
            {
                query = query.Take(takeRows);
            }

            return query;
        }

        private static IOrderedQueryable<T> OrderingHelper<T>(IQueryable<T> source, string propertyName, bool descending, bool anotherLevel)
        {
            ParameterExpression param = Expression.Parameter(typeof(T), string.Empty);
            MemberExpression property = Expression.PropertyOrField(param, propertyName);
            LambdaExpression sort = Expression.Lambda(property, param);

            MethodCallExpression call = Expression.Call(
                typeof(Queryable),
                (!anotherLevel ? "OrderBy" : "ThenBy") + (descending ? "Descending" : string.Empty),
                new[] { typeof(T), property.Type },
                source.Expression,
                Expression.Quote(sort));

            return (IOrderedQueryable<T>)source.Provider.CreateQuery<T>(call);
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, false, false);
        }

        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, true, false);
        }

        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, false, true);
        }

        public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, true, true);
        }

        public static SortDirection GetDirection(string direction)
        {
            SortDirection sortDirection;
            switch (direction.ToLower())
            {
                case null:
                case "":
                case "asc":
                case "ascending":
                    sortDirection = SortDirection.Asc;
                    break;
                case "desc":
                case "descending":
                    sortDirection = SortDirection.Desc;
                    break;
                default:
                    throw new ArgumentException("Invalid parameter", "sortDirection");
            }
            return sortDirection;
        }
    }
}