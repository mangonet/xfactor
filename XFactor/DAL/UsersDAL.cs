﻿using XFactor.DAL.Helpers;
using XFactor.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using XFactor.Tools;
using System.Data.Entity;

namespace XFactor.DAL
{
    public static class UsersDAL
    {
        public static XFactor_Users Create(string facebookID, string email, string fullName, string phone, string singer)
        {
            using (XFactorEntities context = new XFactorEntities()) {
                try
                {
                    XFactor_Users user = Single(facebookID);
                    if (user != null) return user;

                    user = new XFactor_Users
                    {
                        FacebookID = facebookID,
                        Email = email,
                        FullName = fullName,
                        Phone = phone,
                        Singer = singer,
                        Created = DateTime.Now
                    };
                    context.XFactor_Users.Add(user);
                    context.SaveChanges();
                    return user;
                }
                catch (DbEntityValidationException z)
                {
                    string err = "";
                    foreach (var eve in z.EntityValidationErrors)
                    {
                        err += string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            err += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw new Exception(err);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="facebookID"></param>
        /// <param name="includedTables"></param>
        /// <param name="tryGetLastSession">
        /// Trys to get the user by ordering the last session of the user, 
        /// if the user wasn't found only by the facebook id.
        /// </param>
        /// <returns></returns>
        public static XFactor_Users Single(string facebookID)
        {
            using (XFactorEntities context = new XFactorEntities())
            {
                XFactor_Users user = context.XFactor_Users.FirstOrDefault(item => item.FacebookID == facebookID);
                return user;
            }
        }

        public static List<XFactor_Users> All(out int total, IEnumerable<string> includeTables, string facebookID, 
            string email, string fullName, string phone, string singer, string sortField, DALHelpers.SortDirection direction, int skip = 0, int take = 0)
        {
            using (XFactorEntities context = new XFactorEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                IQueryable<XFactor_Users> query = context.XFactor_Users;
                query = query.IncludeMulitple(includeTables);
                query = query.WhereClause( facebookID, email, fullName, phone, singer).
                        SortClause(sortField, direction);

                total = query.Count();           
                query = query.SkipTakeClause(skip, take);

                return query.ToList();
            }
        }

        private static IQueryable<XFactor_Users> WhereClause(this IQueryable<XFactor_Users> query, string facebookID, string email, string fullName, string phone, string singer)
        {
            if (facebookID != null)
            {
                query = query.Where(item => item.FacebookID == facebookID);
            }

            if (email != null)
            {
                query = query.Where(item => item.Email.Contains(email));
            }

            if (fullName != null)
            {
                query = query.Where(item =>
                    item.FullName.Contains(fullName)
                );
            }

            if (phone != null)
            {
                query = query.Where(item =>
                    item.Phone.Contains(phone)
                );
            }

            if (singer != "0")
            {
                query = query.Where(item =>
                    item.Singer.Contains(singer)
                );
            }

            return query;
        }

        private static IQueryable<XFactor_Users> SortClause(this IQueryable<XFactor_Users> query, string sortField, DALHelpers.SortDirection direction)
        {
            if (sortField == "random")
            {
                Random random = new Random();
                query = query.Shuffle(random).AsQueryable();
                random = null;
                return query;
            }
            if (direction == DALHelpers.SortDirection.Desc)
                return query.OrderByDescending(sortField);

            return query.OrderBy(sortField);
        }

        public static void Delete(string fbID)
        {
            using (XFactorEntities context = new XFactorEntities())
            {
                XFactor_Users user = context.XFactor_Users.FirstOrDefault(item => item.FacebookID == fbID);
                context.XFactor_Users.Remove(user);
                context.SaveChanges();
            }
        }
    }
}