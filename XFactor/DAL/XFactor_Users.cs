//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XFactor.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class XFactor_Users
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public string FacebookID { get; set; }
        public string Singer { get; set; }
        public System.DateTime Created { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
