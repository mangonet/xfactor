﻿using XFactor.Models;
using XFactor.Tools;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Net;

namespace XFactor.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            bool isPC = IsPC();
            ViewBag.IsPC = isPC;
            ViewBag.IsLike = true;
            /*if (Request.Form["signed_request"] == null && isPC)
                return Redirect("http://www.facebook.com/pages/Designyourlife/506509046101709?ref=br_tf&id=506509046101709&sk=app_222257817946274");
            else */if (Request.Form["signed_request"] != null) {
                string signedRequest = Request.Form["signed_request"];
                var json = Tools.Tools.DecodePayload(signedRequest);
                ViewBag.IsLike = (json["like"] != null && json["like"].ToString() == "True");
            }
            return View();
        }

        public ActionResult Path()
        {
            Response.Write(Server.MapPath("."));
            Response.End();
            return View();
        }

        protected bool IsPC()
        {
            System.Web.HttpBrowserCapabilitiesBase myBrowserCaps = Request.Browser;
            return !myBrowserCaps.IsMobileDevice;
        }
    }
}
