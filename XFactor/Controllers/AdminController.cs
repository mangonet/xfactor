﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using XFactor.Models;
using XFactor.Tools;
using XFactor.DAL;

namespace XFactor.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/
        private static string AdminUser
        {
            get
            {
                return ConfigurationManager.AppSettings["AdminUser"];
            }
        }

        private static string AdminPass
        {
            get
            {
                return ConfigurationManager.AppSettings["AdminPass"];
            }
        }

        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
                return Redirect("~/Admin/Users");

            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (AdminUser != model.UserName || AdminPass != model.Password)
            {
                ModelState.AddModelError("", "הנתונים שהוזנו אינם נכונים");
                return View(model);
            }

            int timeout = model.RememberMe ? 43829 : 60;
            var ticket = new FormsAuthenticationTicket(model.UserName, model.RememberMe, timeout);
            string encrypted = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
            cookie.Expires = System.DateTime.Now.AddMinutes(timeout);
            Response.Cookies.Add(cookie);

            return Redirect("~/Admin/Users");
        }

        [Authorize(Users = "admin,mangonet")]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            return Redirect("~/");
        }

        [Authorize(Users = "admin,mangonet")]
        public ActionResult Users()
        {
            int total = 0;
            string name = "";
            List<XFactor_Users> users = UsersDAL.All(out total, null, null, null, null, null, "0", "ID", DAL.Helpers.DALHelpers.SortDirection.Asc);
            name = "להקת פיוז'ן";
            total = users.Where(item => item.Singer == name).Count();
            ViewBag.Comp1 = total;
            name = "אבישחר ג'קסון";
            total = users.Where(item => item.Singer == name).Count();
            ViewBag.Comp2 = total;
            name = "אורי שכיב";
            total = users.Where(item => item.Singer == name).Count();
            ViewBag.Comp3 = total;
            name = "בן גולן";
            total = users.Where(item => item.Singer == name).Count();
            ViewBag.Comp4 = total;
            name = "טלי ולירון קרקולי";
            total = users.Where(item => item.Singer == name).Count();
            ViewBag.Comp5 = total;
            name = "האחים אגמי";
            total = users.Where(item => item.Singer == name).Count();
            ViewBag.Comp6 = total;
            name = "יהב טווסי";
            total = users.Where(item => item.Singer == name).Count();
            ViewBag.Comp7 = total;
            name = "נטע רד";
            total = users.Where(item => item.Singer == name).Count();
            ViewBag.Comp8 = total;
            name = "עדן בן זקן";
            total = users.Where(item => item.Singer == name).Count();
            ViewBag.Comp9 = total;
            name = "ענבל ביבי";
            total = users.Where(item => item.Singer == name).Count();
            ViewBag.Comp10 = total;
            name = "רוז פוסטרנט";
            total = users.Where(item => item.Singer == name).Count();
            ViewBag.Comp11 = total;
            name = "תמר פרידמן";
            total = users.Where(item => item.Singer == name).Count();
            ViewBag.Comp12 = total;
            return View();
        }

        [Authorize(Users = "admin,mangonet")]
        public JsonResult UsersGrid()
        {
            Mapper.CreateMap<XFactor_Users, UserView>();
            KendoGridQueryString qs = new KendoGridQueryString(Request);
            int total = 0;
            List<XFactor_Users> result = UsersDAL.All(out total, null,
                qs.FacebookID, qs.Email, qs.FullName, qs.Phone, qs.Singer, qs.SortField, qs.SortDirection, qs.SkipRows, qs.TakeRows);

            List<UserView> viewModel = new List<UserView>();
            foreach (XFactor_Users user in result)
            {
                viewModel.Add(Mapper.Map<XFactor_Users, UserView>(user));
            }

            return Json(new { data = viewModel, total = total }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Users = "admin,mangonet")]
        public ActionResult UsersXls()
        {
            Mapper.CreateMap<XFactor_Users, UserView>();
            KendoGridQueryString qs = new KendoGridQueryString(Request);
            int total = 0;
            List<XFactor_Users> result = UsersDAL.All(out total, null, null, null, null, null, "0", qs.SortField, qs.SortDirection);

            List<UserView> viewModel = Mapper.Map<List<XFactor_Users>, List<UserView>>(result);

            string table = ExcelExport.FormatData(viewModel);
            Response.ClearHeaders();
            Response.AddHeader("Content-Disposition", "attachment; filename=Results.xls");
            Response.ContentType = "application/msexcel";
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(table);
            return File(buffer, Response.ContentType);
        }
    }
}
