﻿using AutoMapper;
using XFactor.DAL;
using XFactor.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

namespace XFactor.Controllers
{
    public class AppController : Controller
    {
        [HttpPost]
        public JsonResult Connect(string fbID, string email, string fullName, string singer, string phone, string singerID)
        {
            string error = "";
            XFactor_Users user = UsersDAL.Create(fbID, email, fullName, phone, singer);
            try
            {
                string path = Tools.Tools.DownloadFile("https://graph.facebook.com/" + fbID + "/picture", singerID, fbID);
            }
            catch (Exception ex)
            {
                error += "FIRST - ";
                error += ex.Message;
                error += " - ";
                error += ex.InnerException.Message;
            }
            try
            {
                Mosaic m = new Mosaic();
                m.DoStart(singerID,fbID);
            }
            catch (Exception ex)
            {
                error += "SECOND - ";
                error += ex.Message;
                error += " - ";
                error += ex.InnerException.Message;
            }
            try
            {
                Tools.Tools.CreateFinal(singer,singerID,fbID);
            }
            catch (Exception ex)
            {
                error += "THIRD - ";
                error += ex.Message;
                error += " - ";
                error += ex.InnerException.Message;
            }
            UserView userModel = MapToUserView(user);
            return Json(new { user = userModel, errors = error });
        }

        [HttpPost]
        public JsonResult GetUser(string fbID)
        {
            var user = UsersDAL.Single(fbID);
            if (user == null)
                return Json(new { user = user });
            
            UserView userModel = MapToUserView(user);
            return Json(new { user = userModel });
        }

        [HttpPost]
        public JsonResult DeleteUser(string fbID)
        {
            UsersDAL.Delete(fbID);
            return Json(new { user = "" });
        }


        protected UserView MapToUserView(XFactor_Users user)
        {
            Mapper.CreateMap<XFactor_Users, UserView>();
            UserView userModel = Mapper.Map<XFactor_Users, UserView>(user);
            return userModel;
        }
    }
}
