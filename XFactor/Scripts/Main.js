﻿$(document).ready(function () {
    App.init();
    Popup.init();
});

var User = {
    fbid: "",
    accessToken: "",
    liked: false,
    fullName: "",
    email: "",
    phone: "",
    fillData: function (userObj) {
        User.fullName = userObj.user.FirstName + " " + userObj.user.LastName;
        User.email = userObj.user.Email;
        User.phone = userObj.user.Phone;
    }
}

var fb = {
    init: function () {
        FB.Event.subscribe('edge.create',
		    function (response) {
		        App.goto("howtoplay");
		    }
		);
        fb.isLoggedIn();
    },
    isLoggedIn: function () {
        FB.getLoginStatus(function (response) {
            if (response.status === "connected") {
                User.fbid = response.authResponse.userID;
                User.accessToken = response.authResponse.accessToken;
                $.post("App/GetUser", { fbID: User.fbid }, function (e) {
                    if (e.user != null) {
                        User.fillData(e);
                        App.goto("gallery");
                    } else {
                        App.goto("welcome");
                    }
                });
            } else {
                App.goto("welcome");
            }
        });
    },
    login: function () {
        FB.login(function (response) {
            if (response.authResponse) {
                User.fbid = response.authResponse.userID;
                User.accessToken = response.authResponse.accessToken;
                FB.api('/me', function (response) {
                    User.email = response.email;
                    User.fullName = response.first_name + " " + response.last_name;
                });
                GA.report("Approved application");
                App.goto("photo");
            }
        }, { scope: "email" });
    },
    share: function () {
        GA.report("Share");
        var obj = {
            method: 'feed',
            redirect_uri: "https://www.facebook.com/pages/Tests/506509046101709?id=506509046101709&sk=app_1375849119337401",
            link: "https://www.facebook.com/pages/Tests/506509046101709?id=506509046101709&sk=app_1375849119337401",
            picture: "https://sslsecured.nethost.co.il/XFactor/images/200x200.jpg",
            name: "X FACTOR - Isracard",
            caption: "אני השתתפתי במשחק הקולאז' של ישראכרט ואולי אזכה בכרטיס להופעות החיות של אקס פאקטור! רוצים להצטרף אליי להופעה? כנסו ושחקו גם אתם.",
            description: ""
        };
        FB.ui(obj);
    }
};

var App = {
    curPage: "temp",
    isMobile: function () {
        return $(window).width() < 760;
    },
    init: function () {
        $("#welcome #play").click(function () {
            App.goto("howtoplay");
        });
        $("#form .submit-btn").click(App.sendData);
        $("#howtoplay img").not(".off").click(App.chooseSinger);
        $("#photo #mainPhoto").click(function () {
            App.goto("form");
        });
        $("#form #send").click(App.sendData);
        $("#done #share").click(fb.share);
        $("#done #skip").click(function () {
            App.goto("thankyou");
        });
        $(".togallery").click(function () {
            App.goto("gallery");
        });
        $("#gallery img").not(".off").click(function () {
            var singer = $(this).attr("src").replace("small", "big");
            $("#galleryPhoto img").attr("src", singer) + "?" + randDate();

            var singerID = singer.replace("images/singers/big/", "").replace(".jpg", "");
            var name = App.getSingerName(singerID);
            GA.report("Gallery", name);
            App.goto("galleryPhoto");
        });
        $("#gallery .delete").click(function () {
            $.post("App/DeleteUser", { fbID: User.fbid });
        });
    },
    chooseSinger: function () {
        User.singer = $(this).attr("src");
        fb.login();
    },
    goto: function (page) {
        if (App.curPage == page) return;
        switch (page) {
            case "welcome":
                if (User.liked) {
                    $("#play").show();
                    $("#welcome").addClass("like");
                    GA.report("Welcome - Prelike");
                }
                else {
                    $("#like").show();
                    GA.report("Welcome - Postlike");
                }
                break;
            case "photo":
                $("#photo #profilePhoto").attr("src", "https://graph.facebook.com/" + User.fbid + "/picture");
                $("#photo #mainPhoto").attr("src", User.singer.replace("small", "big"));
                break;
            case "form":
                App.fillForm();
                break;
            case "done":
                $("#done img").attr("src", User.singer.replace("small", "big") + "?"+randDate());
                GA.report("Vote",User.singerStr);
                break;
            case "howtoplay":
                GA.report("How to play");
                break;
            case "gallery":
                GA.report("Gallery","Main");
                break;
        }
        $("#" + App.curPage).fadeOut(function () {
            App.curPage = page;
            $("#" + page).fadeIn();
        });
    },
    fillForm: function () {
        $("#form #FullName").val(User.fullName);
        $("#form #EMail").val(User.email);
    },
    sendData: function () {
        User.fullName = $("#form #FullName").val();
        User.email = $("#form #EMail").val();
        User.phone = $("#form #Phone").val();
        if (User.fullName.length < 3) {
            Popup.show("נא הזן שם מלא");
            return false;
        }
        var reg = new RegExp('^[0-9\-]+$');
        if (User.phone.length < 9 || User.phone.length > 11 || !reg.test(User.phone)) {
            Popup.show("נא הזן טלפון תקין");
            return false;
        }
        if ($("#form #Agreement:checked").length == 0) {
            Popup.show("יש לאשר את התקנון על מנת להשתתף");
            return false;
        }
        if (User.email.indexOf("@") == -1 || User.email.indexOf(".") == -1) {
            Popup.show("נא הזן דוא\"ל");
            return false;
        }
        User.singerID = User.singer.replace("images/singers/small/", "").replace(".jpg","");
        User.singerStr = App.getSingerName(User.singerID);
        $.post("App/Connect", {
            Email: User.email,
            fbid: User.fbid,
            FullName: User.fullName,
            Phone: User.phone,
            Singer: User.singerStr,
            SingerID: User.singerID
        }, function (e) {
            if (e.status == "not_valid") {
                alert(e.errors[0]);
                return false;
            } else {
                App.goto("done");
            }
        });
    },
    getSingerName: function(id) {
        switch (id) {
            case "01":
                return "להקת פיוז'ן";
                break;
            case "02":
                return "אבישחר ג'קסון";
                break;
            case "03":
                return "אורי שכיב";
                break;
            case "04":
                return "בן גולן";
                break;
            case "05":
                return "טלי ולירון קרקולי";
                break;
            case "06":
                return "האחים אגמי";
                break;
            case "07":
                return "יהב טווסי";
                break;
            case "08":
                return "נטע רד";
                break;
            case "09":
                return "עדן בן זקן";
                break;
            case "10":
                return "ענבל ביבי";
                break;
            case "11":
                return "רוז פוסטרנט";
                break;
            case "12":
                return "תמר פרידמן";
                break;
        }
    }
}

var GA = {
    page: function (path) {
        ga('send', 'pageview', path);
    },
    event: function (cat, act, lbl) {
        if (lbl) {
            ga('send', 'event', cat, act, lbl);
        }
        else {
            ga('send', 'event', cat, act);
        }
    },
    report: function (act, lbl) {
        if (lbl != null) lbl = lbl.replace("/", "-");
        var cat = "PC";
        //if (App.isMobile()) cat = "Mobile";
        var path = "/" + cat + "/" + act;
        if (lbl) path += "/" + lbl;
        GA.page(path);
        GA.event(cat, act, lbl);
    }
}

var Popup = {
    to:null,
    init: function() {
        $("#overlay").click(Popup.hide);
        $("#popup input").click(Popup.hide);
    },
    show: function (msg) {
        $("#popup p").text(msg);
        $("#overlay").fadeIn();
        $("#popup").fadeIn();
        Popup.to = setTimeout(function () {
            Popup.hide();
        }, 3000);
    },
    hide: function () {
        $("#overlay").fadeOut();
        $("#popup").fadeOut();
        clearTimeout(Popup.to);
    }
}

function randDate() {
    var d = new Date();
    return d.getTime();
}