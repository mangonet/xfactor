﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XFactor.Tools;

namespace XFactor.Models
{
    public class UserView
    {
        public int ID { get; set; }

        public string FacebookID { get; set; }

        public DateTime Created { get; set; }

        public string CreatedStr { get { return Created.ToEUDateFormat(); } }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Singer { get; set; }
    }
}