﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using XFactor.Tools;
using System.ComponentModel;

namespace XFactor.Models
{
    public class User
    {
        public int ID { get; set; }

        [Required]
        public string FacebookID { get; set; }

        public DateTime Created { get; set; }
        public string CreatedStr { get { return Created.ToEUDateFormat(); } }

        [Required(ErrorMessage = "נא להזין שם מלא")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "נא להזין אי-מייל")]
        [RegularExpression(@"^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$", ErrorMessage = "הכתובת אינה תקינה")]
        public string Email { get; set; }

        [RegularExpression(@"^\d+(\d)?$", ErrorMessage = "מספר הטלפון אינו תקין")]
        public string Phone { get; set; }

        public string Singer { get; set; }

        public User() { }
    }    
}