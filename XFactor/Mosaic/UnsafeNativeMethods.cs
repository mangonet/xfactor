﻿namespace XFactor
{
    using System;
    using System.Runtime.ConstrainedExecution;
    using System.Runtime.InteropServices;

    internal static class UnsafeNativeMethods
    {
        #region Methods - Extern

        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail), DllImport("kernel32.dll", SetLastError = true)]
        internal static extern unsafe void* VirtualAlloc(void* address, UIntPtr numBytes, int commitOrReserve, int pageProtectionMode);

        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail), DllImport("kernel32.dll", SetLastError = true)]
        internal static extern unsafe bool VirtualFree(void* address, UIntPtr numBytes, int pageFreeMode);

        #endregion Methods - Extern
    }
}