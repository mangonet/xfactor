﻿namespace System.Drawing
{
    using System.Collections.Generic;
    using System.Linq;

    public static class EnumerableExtender
    {
        #region Methods - Private

        private static IEnumerable<T> Scramble<T>(IEnumerable<T> values, Random random)
        {
            var array = values.ToArray();
            var flags = new bool[array.Length];
            while (flags.Any(f => !f))
            {
                var i = random.Next(0, array.Length);
                if (flags[i]) continue;
                flags[i] = true;
                yield return array[i];
            }
        }

        #endregion Methods - Private

        #region Methods

        public static IEnumerable<T> Rotate<T>(this IEnumerable<T> values, Random random)
        {
            while (true)
            {
                var permutations = Scramble(values, random);
                foreach (var item in permutations)
                    yield return item;
            }
        }

        #endregion Methods
    }
}