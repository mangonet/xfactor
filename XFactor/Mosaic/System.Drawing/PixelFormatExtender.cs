﻿namespace System.Drawing
{
    using System.Drawing.Imaging;

    public static class PixelFormatExtender
    {
        #region Methods

        public static int GetPixelFormatSize(this PixelFormat pixelFormat)
        {
            return ((((int)pixelFormat) >> 8) & 0xff);
        }

        public static int BytesPerPixel(this PixelFormat pixelFormat)
        {
            return pixelFormat.GetPixelFormatSize() / 8;
        }

        [CLSCompliant(false)]
        public static ulong CalculateByteCount(this PixelFormat pixelFormat, Size size)
        {
            return (ulong)size.Width * (ulong)size.Height * (ulong)pixelFormat.BytesPerPixel();
        }

        public static int CalculateStride(this PixelFormat pixelFormat, Size size)
        {
            var bytesPerPixel = pixelFormat.BytesPerPixel();
            var padding = 4 - ((size.Width * bytesPerPixel) % 4);
            padding = (padding == 4 ? 0 : padding);
            var stride = (size.Width * bytesPerPixel) + padding;
            return stride;
        }

        #endregion Methods
    }
}