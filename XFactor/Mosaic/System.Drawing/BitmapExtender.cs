﻿namespace System.Drawing
{
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;

    public static class BitmapExtender
    {
        #region Methods - Private

        public static ImageCodecInfo GetEncoder(string filename)
        {
            var encoders = ImageCodecInfo.GetImageEncoders();
            var extension = Path.GetExtension(filename).ToUpperInvariant();
            return encoders.Single(e => e.FilenameExtension.Contains(extension));
        }

        #endregion Methods - Private

        #region Methods

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Image CropSquare(this Image image)
        {
            int size, x, y;
            if (image.Height > image.Width)
            {
                size = image.Width;
                x = 0;
                y = -(int)Math.Round((image.Height - size) / 2f);
            }
            else
            {
                size = image.Height;
                x = -(int)Math.Round((image.Width - size) / 2f);
                y = 0;
            }
            var croppedSquare = new Bitmap(size, size, image.PixelFormat);
            try
                {
                    using (var graphics = Graphics.FromImage(croppedSquare))
                    {
                            graphics.DrawImage(image, x, y, image.Width, image.Height);
                    }
                }
            catch (Exception e)
            {

            }
            return croppedSquare;
        }

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Image CropY(this Image image, int width, int height)
        {
            int size, x, y;
            x = 0;
            y = 0;// (image.Height - height);
            var cropped = new Bitmap(width, height, image.PixelFormat);
            using (var graphics = Graphics.FromImage(cropped))
                graphics.DrawImage(image, x, y, image.Width, image.Height);
            return cropped;
        }

        public static void FillWithTransparentColor(this Image image, Color color, int alpha)
        {
            using (var graphics = Graphics.FromImage(image))
            using (var brush = new SolidBrush(Color.FromArgb(alpha, color)))
                graphics.FillRectangle(brush, 0, 0, image.Width, image.Height);
        }

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Bitmap Scale(this Image image, int width, int height)
        {
            var scaled = new Bitmap(width, height, image.PixelFormat);
            try
            {
                using (var graphics = Graphics.FromImage(scaled))
                {
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.DrawImage(image, -1, -1, width + 1, height + 1);
                }
            }
            catch (Exception e)
            {
            }
            return scaled;
        }

        public static void TypeAwareSave(this Image image, string filename)
        {
            //filename = filename.Replace(".jpg", "_.jpg");
            var encoder = GetEncoder(filename);
            image.Save(filename, encoder, null);
        }

        #endregion Methods
    }
}