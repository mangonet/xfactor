﻿namespace System.Drawing
{
    using System.Collections.Generic;

    public static class RectangleExtender
    {
        #region Methods

        public static IEnumerable<Point> Points(this Rectangle rectangle)
        {
            for (int x = rectangle.Left; x < rectangle.Right; x++)
                for (int y = rectangle.Top; y < rectangle.Bottom; y++)
                    yield return new Point(x, y);
        }

        #endregion Methods
    }
}