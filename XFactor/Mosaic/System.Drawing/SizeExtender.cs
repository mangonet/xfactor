﻿namespace System.Drawing
{
    using System.Collections.Generic;

    public static class SizeExtender
    {
        #region Methods

        public static Size GetGridSize(this Size size, int sectorSize)
        {
            var width = size.Width / sectorSize;
            if (width * sectorSize < size.Width) width++;
            var height = size.Height / sectorSize;
            if (height * sectorSize < size.Height) height++;
            return new Size(width, height);
        }

        public static IEnumerable<Point> GetSectors(this Size gridSize)
        {
            for (int x = 0; x < gridSize.Width; x++)
                for (int y = 0; y < gridSize.Height; y++)
                    yield return new Point(x, y);
        }

        public static Size Scale(this Size size, float scale)
        {
            return new Size((int)Math.Round(size.Width * scale), (int)Math.Round(size.Height * scale));
        }

        #endregion Methods
    }
}