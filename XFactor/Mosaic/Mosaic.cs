﻿namespace XFactor
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Threading.Tasks;

    public class Mosaic
    {
        #region Constants

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private const PixelFormat DefaultPixelFormat = PixelFormat.Format24bppRgb;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private const string IconFilenameFormat = "{0}_{1}{2}";

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private const string IconFilenameSuffixFormat = "_{0}";

        #endregion Constants

        #region Fields

        private int _SuppressScaleChanges;

        #endregion Fields

        #region Properties

        private int? Alpha
        {
            get
            {
                return 200;
            }
        }

        private string Destination
        {
            get { return ConfigurationManager.AppSettings["Temp"] + singerID + "_"+fbid+".jpg"; }
        }

        private string Temp
        {
            get { return ConfigurationManager.AppSettings["Temp"]; }
        }

        private int? DestinationHeight
        {
            get
            {
                return 1200;
            }
        }

        private float? DestinationScale
        {
            get
            {
                return 1;
            }
        }

        private int? DestinationWidth
        {
            get
            {
                return 960;
            }
        }

        private string Icons
        {
            get { return ConfigurationManager.AppSettings["Icons"] + singerID + "\\"; }
        }

        private int? IconSize
        {
            get
            {
                return 30;
            }
        }

        private string Images
        {
            get { return ConfigurationManager.AppSettings["Profiles"] + singerID + "\\"; }
        }

        private string Source
        {
            get { return ConfigurationManager.AppSettings["Source"] + singerID + ".jpg"; }
        }

        private string singerID;
        private string fbid;

        private Size? SourceSize { get; set; }

        #endregion Properties

        #region Methods - General

        private static float? ParseFloat(string value)
        {
            float f;
            var parsed = float.TryParse(value, out f);
            if (parsed)
                return f;
            return null;
        }

        private static int? ParseInt(string value)
        {
            int i;
            var parsed = int.TryParse(value, out i);
            if (parsed)
                return i;
            return null;
        }

        #endregion Methods - General

        #region Methods - Operation

        public void DoStart(string id,string fid)
        {
            singerID = id;
            fbid = fid;
            GenerateIcons();
            CreateMosaic();
        }

        #endregion Methods - Operation

        #region Methods - Icons

        private static string CreateIconFilename(string path, int size, string imageFilename)
        {
            var filename = Path.GetFileNameWithoutExtension(imageFilename);
            var extension = Path.GetExtension(imageFilename);
            var iconFilename = string.Format(CultureInfo.InvariantCulture, IconFilenameFormat, filename, size, extension);
            return Path.Combine(path, iconFilename);
        }

        private void GenerateIcon(string imageFilename)
        {
            var iconFilename = CreateIconFilename(Icons, IconSize.Value, imageFilename);
            if (!File.Exists(iconFilename))
            {
                using (var image = (Bitmap)Bitmap.FromFile(imageFilename))
                using (var square = image.CropSquare())
                using (var icon = square.Scale(IconSize.Value, IconSize.Value))
                    icon.TypeAwareSave(iconFilename);
            }
        }

        private void GenerateIcons()
        {
            var imageFilenames = Directory.GetFiles(Images,"*.jpg");
            Parallel.ForEach(imageFilenames, GenerateIcon);
        }

        #endregion Methods - Icons

        #region Methods - Mosaic

        private static unsafe void DrawBitmap(byte* start, int stride, int width, int height, int bytesPerPixel, Bitmap bitmap, Rectangle rectangle)
        {
            var sourceBd = bitmap.LockBits(new Rectangle(Point.Empty, bitmap.Size), ImageLockMode.ReadOnly, bitmap.PixelFormat);
            try
            {
                var sourceStart = (byte*)sourceBd.Scan0.ToPointer();
                var sourceStride = sourceBd.Stride;
                Parallel.ForEach(rectangle.Points(), item =>
                {
                    var color = GetPixel(sourceStart, sourceStride, item.X - rectangle.Left, item.Y - rectangle.Top, bytesPerPixel);
                    if (item.X < width && item.Y < height)
                        SetPixel(start, stride, item.X, item.Y, bytesPerPixel, color);
                });
            }
            finally
            {
                bitmap.UnlockBits(sourceBd);
            }
        }

        private static IEnumerable<string> GetIconFilenames(string iconsDirectory, int iconSize)
        {
            var suffix = string.Format(CultureInfo.InvariantCulture, IconFilenameSuffixFormat, iconSize);
            foreach (var imageFilename in Directory.EnumerateFiles(iconsDirectory))
            {
                var filename = Path.GetFileNameWithoutExtension(imageFilename);
                if (filename.EndsWith(suffix, StringComparison.OrdinalIgnoreCase))
                    yield return imageFilename;
            }
        }

        private static unsafe Color GetPixel(byte* start, int stride, int x, int y, int bytesPerPixel)
        {
            var ptr = GetPtr(start, stride, x, y, bytesPerPixel);
            return Color.FromArgb(ptr[2], ptr[1], ptr[0]);
        }

        private static unsafe byte* GetPtr(byte* start, int stride, int x, int y, int bytesPerPixel)
        {
            return (byte*)start + y * stride + x * bytesPerPixel;
        }

        private static IEnumerable<Tuple<Point, Color, string>> GetSectors(Size gridSize, Bitmap bitmap, IEnumerable<string> filenames)
        {
            var grid = gridSize.GetSectors();
            return (from sector in grid
                    select new
                    {
                        Sector = sector,
                        Color = bitmap.GetPixel(sector.X, sector.Y)
                    })
                    .Zip(filenames, (d, filename) => new Tuple<Point, Color, string>(d.Sector, d.Color, filename));
        }

        private static unsafe void SetPixel(byte* start, int stride, int x, int y, int bytesPerPixel, Color color)
        {
            var ptr = GetPtr(start, stride, x, y, bytesPerPixel);
            ptr[2] = color.R;
            ptr[1] = color.G;
            ptr[0] = color.B;
        }

        private void CreateMosaic()
        {
            var newDimensions = new Size(DestinationWidth.Value, DestinationHeight.Value);
            
                var iconFilenames = GetIconFilenames(Icons, IconSize.Value);
                using (var sourceBitmap = (Bitmap)Bitmap.FromFile(Source))
                    CreatePhotographicMosaic(sourceBitmap, iconFilenames, newDimensions, Alpha.Value, IconSize.Value, Destination);
        }

        private unsafe void CreatePhotographicMosaic(Bitmap source, IEnumerable<string> iconFilenames, Size size, int alpha, int iconSize, string filename)
        {
            var status = 0;
            void* address = null;
            var nativeImageHandle = new HandleRef(null, IntPtr.Zero);
            try
            {
                var bytesPerPixel = DefaultPixelFormat.BytesPerPixel();
                var totalBytes = DefaultPixelFormat.CalculateByteCount(size);
                var numBytes = new UIntPtr(totalBytes);
                address = UnsafeNativeMethods.VirtualAlloc(null, numBytes, 0x1000 | 0x2000, 0x04);
                var stride = DefaultPixelFormat.CalculateStride(size);
                var rectangle = new Rectangle(Point.Empty, size);
                var random = new Random((int)DateTime.UtcNow.Ticks);
                var randomlyRotatedFilenames = iconFilenames.ToArray().Rotate(random);
                var gridSize = rectangle.Size.GetGridSize(iconSize);
                using (var scaledDownBitmap = source.Scale(gridSize.Width, gridSize.Height))
                {
                    var sectors = GetSectors(gridSize, scaledDownBitmap, randomlyRotatedFilenames);
                    Parallel.ForEach(sectors, sector => ProcessSector((byte*)address, stride, size.Width, size.Height, alpha, iconSize, bytesPerPixel, sector));
                }
                var nativeImage = IntPtr.Zero;
                status = SafeNativeMethods.GdipCreateBitmapFromScan0(size.Width, size.Height, stride, (int)PixelFormat.Format24bppRgb, new HandleRef(this, new IntPtr(address)), out nativeImage);
                Debug.Assert(status == 0);
                nativeImageHandle = new HandleRef(this, nativeImage);
                status = SafeNativeMethods.GdipBitmapSetResolution(nativeImageHandle, source.HorizontalResolution, source.VerticalResolution);
                Debug.Assert(status == 0);
                var encoder = BitmapExtender.GetEncoder(filename);
                var clsid = encoder.Clsid;
                //INFO: For very large images (totalBytes: 4GB+), this fails.
                status = SafeNativeMethods.GdipSaveImageToFile(nativeImageHandle, filename, ref clsid, new HandleRef(this, IntPtr.Zero));
                Debug.Assert(status == 0);
            }
            finally
            {
                status = SafeNativeMethods.GdipDisposeImage(nativeImageHandle);
                Debug.Assert(status == 0);
                if (null != address)
                    UnsafeNativeMethods.VirtualFree(address, UIntPtr.Zero, 0x8000);
            }
        }

        private unsafe void ProcessSector(byte* start, int stride, int width, int height, int alpha, int iconSize, int bytesPerPixel, Tuple<Point, Color, string> section)
        {
            var x = section.Item1.X * iconSize;
            var y = section.Item1.Y * iconSize;
            using (var icon = (Bitmap)Bitmap.FromFile(section.Item3))
            {
                icon.FillWithTransparentColor(section.Item2, alpha);
                DrawBitmap(start, stride, width, height, bytesPerPixel, icon, new Rectangle(x, y, iconSize, iconSize));
            }
        }

        #endregion Methods - Mosaic
    }
}